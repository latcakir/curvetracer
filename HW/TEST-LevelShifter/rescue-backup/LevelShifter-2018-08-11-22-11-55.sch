EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:suf_regulator
LIBS:suf_led_driver
LIBS:LevelShifter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM324 U1
U 3 1 5B693252
P 3450 1575
F 0 "U1" H 3450 1775 50  0000 L CNN
F 1 "OPA4197" H 3400 1375 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3400 1675 50  0001 C CNN
F 3 "" H 3500 1775 50  0001 C CNN
	3    3450 1575
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 5B69332A
P 3600 1950
F 0 "R11" V 3680 1950 50  0000 C CNN
F 1 "100K" V 3600 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3530 1950 50  0001 C CNN
F 3 "" H 3600 1950 50  0001 C CNN
	1    3600 1950
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 5B69349C
P 2875 1475
F 0 "R5" V 2955 1475 50  0000 C CNN
F 1 "100K" V 2875 1475 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2805 1475 50  0001 C CNN
F 3 "" H 2875 1475 50  0001 C CNN
	1    2875 1475
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5B6934E9
P 2875 1675
F 0 "R6" V 2955 1675 50  0000 C CNN
F 1 "100K" V 2875 1675 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2805 1675 50  0001 C CNN
F 3 "" H 2875 1675 50  0001 C CNN
	1    2875 1675
	0    1    1    0   
$EndComp
$Comp
L R R9
U 1 1 5B69352D
P 3075 2150
F 0 "R9" V 3155 2150 50  0000 C CNN
F 1 "100K" V 3075 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3005 2150 50  0001 C CNN
F 3 "" H 3075 2150 50  0001 C CNN
	1    3075 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3025 1475 3150 1475
Wire Wire Line
	3025 1675 3150 1675
Wire Wire Line
	3125 1675 3125 1950
Wire Wire Line
	3125 1950 3450 1950
Connection ~ 3125 1675
Wire Wire Line
	3750 1950 3800 1950
Wire Wire Line
	3800 1950 3800 1575
Wire Wire Line
	3750 1575 3900 1575
Wire Wire Line
	3075 2000 3075 1475
Connection ~ 3075 1475
$Comp
L GND #PWR01
U 1 1 5B6935D7
P 3075 2400
F 0 "#PWR01" H 3075 2150 50  0001 C CNN
F 1 "GND" H 3075 2250 50  0000 C CNN
F 2 "" H 3075 2400 50  0001 C CNN
F 3 "" H 3075 2400 50  0001 C CNN
	1    3075 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 2300 3075 2400
$Comp
L LM324 U1
U 1 1 5B693750
P 2000 1675
F 0 "U1" H 2000 1875 50  0000 L CNN
F 1 "OPA4197" H 1950 1475 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1950 1775 50  0001 C CNN
F 3 "" H 2050 1875 50  0001 C CNN
	1    2000 1675
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B69380A
P 1525 1325
F 0 "R1" V 1605 1325 50  0000 C CNN
F 1 "100K" V 1525 1325 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1455 1325 50  0001 C CNN
F 3 "" H 1525 1325 50  0001 C CNN
	1    1525 1325
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 5B693885
P 1525 1800
F 0 "R2" V 1605 1800 50  0000 C CNN
F 1 "100K" V 1525 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1455 1800 50  0001 C CNN
F 3 "" H 1525 1800 50  0001 C CNN
	1    1525 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 1775 1650 1775
Wire Wire Line
	1650 1775 1650 2050
Wire Wire Line
	1650 2050 2300 2050
Wire Wire Line
	2300 2050 2300 1675
Wire Wire Line
	2300 1675 2725 1675
Connection ~ 2300 1675
Wire Wire Line
	1525 1475 1525 1650
Wire Wire Line
	1700 1575 1525 1575
Connection ~ 1525 1575
$Comp
L GND #PWR02
U 1 1 5B693942
P 1525 2050
F 0 "#PWR02" H 1525 1800 50  0001 C CNN
F 1 "GND" H 1525 1900 50  0000 C CNN
F 2 "" H 1525 2050 50  0001 C CNN
F 3 "" H 1525 2050 50  0001 C CNN
	1    1525 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 1950 1525 2050
$Comp
L VCC #PWR03
U 1 1 5B693A07
P 1775 1000
F 0 "#PWR03" H 1775 850 50  0001 C CNN
F 1 "VCC" H 1775 1150 50  0000 C CNN
F 2 "" H 1775 1000 50  0001 C CNN
F 3 "" H 1775 1000 50  0001 C CNN
	1    1775 1000
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR04
U 1 1 5B693A2B
P 1275 1000
F 0 "#PWR04" H 1275 850 50  0001 C CNN
F 1 "VDD" H 1275 1150 50  0000 C CNN
F 2 "" H 1275 1000 50  0001 C CNN
F 3 "" H 1275 1000 50  0001 C CNN
	1    1275 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 1175 1525 1150
Wire Wire Line
	1275 1000 1275 1050
Wire Wire Line
	1775 1000 1775 1050
$Comp
L VCC #PWR05
U 1 1 5B693BC5
P 1900 1275
F 0 "#PWR05" H 1900 1125 50  0001 C CNN
F 1 "VCC" H 1900 1425 50  0000 C CNN
F 2 "" H 1900 1275 50  0001 C CNN
F 3 "" H 1900 1275 50  0001 C CNN
	1    1900 1275
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 5B693BEE
P 3350 1200
F 0 "#PWR06" H 3350 1050 50  0001 C CNN
F 1 "VCC" H 3350 1350 50  0000 C CNN
F 2 "" H 3350 1200 50  0001 C CNN
F 3 "" H 3350 1200 50  0001 C CNN
	1    3350 1200
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR07
U 1 1 5B693C17
P 1900 2125
F 0 "#PWR07" H 1900 1975 50  0001 C CNN
F 1 "VDD" H 1900 2275 50  0000 C CNN
F 2 "" H 1900 2125 50  0001 C CNN
F 3 "" H 1900 2125 50  0001 C CNN
	1    1900 2125
	-1   0    0    1   
$EndComp
$Comp
L VDD #PWR08
U 1 1 5B693C68
P 3350 2025
F 0 "#PWR08" H 3350 1875 50  0001 C CNN
F 1 "VDD" H 3350 2175 50  0000 C CNN
F 2 "" H 3350 2025 50  0001 C CNN
F 3 "" H 3350 2025 50  0001 C CNN
	1    3350 2025
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 1875 3350 2025
Wire Wire Line
	1900 1975 1900 2125
$Comp
L ICL7660S U2
U 1 1 5B693CE4
P 2550 5750
F 0 "U2" H 2550 5500 60  0000 C CNN
F 1 "ICL7660S" H 2550 6000 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2550 5750 60  0001 C CNN
F 3 "" H 2550 5750 60  0000 C CNN
	1    2550 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 1275 1900 1375
Wire Wire Line
	3350 1200 3350 1275
$Comp
L C C2
U 1 1 5B69D6DA
P 3050 6100
F 0 "C2" H 3075 6200 50  0000 L CNN
F 1 "10uF" H 3075 6000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3088 5950 50  0001 C CNN
F 3 "" H 3050 6100 50  0001 C CNN
	1    3050 6100
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B69D755
P 1725 5850
F 0 "C1" H 1750 5950 50  0000 L CNN
F 1 "10uF" H 1750 5750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1763 5700 50  0001 C CNN
F 3 "" H 1725 5850 50  0001 C CNN
	1    1725 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5B69D7E8
P 1925 6050
F 0 "#PWR09" H 1925 5800 50  0001 C CNN
F 1 "GND" H 1925 5900 50  0000 C CNN
F 2 "" H 1925 6050 50  0001 C CNN
F 3 "" H 1925 6050 50  0001 C CNN
	1    1925 6050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B69D823
P 3050 6325
F 0 "#PWR010" H 3050 6075 50  0001 C CNN
F 1 "GND" H 3050 6175 50  0000 C CNN
F 2 "" H 3050 6325 50  0001 C CNN
F 3 "" H 3050 6325 50  0001 C CNN
	1    3050 6325
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5B69D851
P 3325 6125
F 0 "#PWR011" H 3325 5875 50  0001 C CNN
F 1 "GND" H 3325 5975 50  0000 C CNN
F 2 "" H 3325 6125 50  0001 C CNN
F 3 "" H 3325 6125 50  0001 C CNN
	1    3325 6125
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR012
U 1 1 5B69D87F
P 3475 5800
F 0 "#PWR012" H 3475 5650 50  0001 C CNN
F 1 "VDD" H 3475 5950 50  0000 C CNN
F 2 "" H 3475 5800 50  0001 C CNN
F 3 "" H 3475 5800 50  0001 C CNN
	1    3475 5800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR013
U 1 1 5B69D8BA
P 1975 5525
F 0 "#PWR013" H 1975 5375 50  0001 C CNN
F 1 "VCC" H 1975 5675 50  0000 C CNN
F 2 "" H 1975 5525 50  0001 C CNN
F 3 "" H 1975 5525 50  0001 C CNN
	1    1975 5525
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR014
U 1 1 5B69D8E8
P 3075 5525
F 0 "#PWR014" H 3075 5375 50  0001 C CNN
F 1 "VCC" H 3075 5675 50  0000 C CNN
F 2 "" H 3075 5525 50  0001 C CNN
F 3 "" H 3075 5525 50  0001 C CNN
	1    3075 5525
	1    0    0    -1  
$EndComp
Wire Wire Line
	1975 5525 1975 5600
Wire Wire Line
	1975 5600 2050 5600
Wire Wire Line
	3000 5600 3075 5600
Wire Wire Line
	3075 5600 3075 5525
Wire Wire Line
	2050 5700 1725 5700
Wire Wire Line
	2050 5900 2050 6000
Wire Wire Line
	2050 6000 1725 6000
Wire Wire Line
	1925 6050 1925 5800
Wire Wire Line
	1925 5800 2050 5800
Wire Wire Line
	3000 5900 3475 5900
Wire Wire Line
	3475 5900 3475 5800
Wire Wire Line
	3000 5800 3325 5800
Wire Wire Line
	3325 5800 3325 6125
Wire Wire Line
	3050 5900 3050 5950
Connection ~ 3050 5900
Wire Wire Line
	3050 6250 3050 6325
$Comp
L CONN_01X06 J1
U 1 1 5B6B18DD
P 6100 1600
F 0 "J1" H 6100 1950 50  0000 C CNN
F 1 "CONN_01X06" V 6200 1600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 6100 1600 50  0001 C CNN
F 3 "" H 6100 1600 50  0001 C CNN
	1    6100 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5B6B192A
P 5825 1950
F 0 "#PWR015" H 5825 1700 50  0001 C CNN
F 1 "GND" H 5825 1800 50  0000 C CNN
F 2 "" H 5825 1950 50  0001 C CNN
F 3 "" H 5825 1950 50  0001 C CNN
	1    5825 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1450 5825 1450
Wire Wire Line
	5825 1450 5825 1950
$Comp
L VCC #PWR016
U 1 1 5B6B19A9
P 5825 1275
F 0 "#PWR016" H 5825 1125 50  0001 C CNN
F 1 "VCC" H 5825 1425 50  0000 C CNN
F 2 "" H 5825 1275 50  0001 C CNN
F 3 "" H 5825 1275 50  0001 C CNN
	1    5825 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1350 5825 1350
Wire Wire Line
	5825 1350 5825 1275
Text GLabel 5725 1550 0    40   Input ~ 0
IN1
Text GLabel 5725 1650 0    40   Input ~ 0
OUT1
Text GLabel 5725 1750 0    40   Input ~ 0
IN2
Text GLabel 5725 1850 0    40   Input ~ 0
OUT2
Wire Wire Line
	5725 1550 5900 1550
Wire Wire Line
	5900 1650 5725 1650
Wire Wire Line
	5725 1750 5900 1750
Wire Wire Line
	5900 1850 5725 1850
Text GLabel 2600 1475 0    40   Input ~ 0
IN1
Text GLabel 3900 1575 2    40   Input ~ 0
OUT1
Wire Wire Line
	2600 1475 2725 1475
Connection ~ 3800 1575
$Comp
L Jumper_NC_Dual JP1
U 1 1 5B6B1D93
P 1525 1050
F 0 "JP1" H 1575 950 50  0000 L CNN
F 1 "-+" H 1525 1150 50  0000 C BNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 1525 1050 50  0001 C CNN
F 3 "" H 1525 1050 50  0001 C CNN
	1    1525 1050
	1    0    0    -1  
$EndComp
$Comp
L LM324 U1
U 4 1 5B6B3716
P 3450 3550
F 0 "U1" H 3450 3750 50  0000 L CNN
F 1 "OPA4197" H 3400 3350 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3400 3650 50  0001 C CNN
F 3 "" H 3500 3750 50  0001 C CNN
	4    3450 3550
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5B6B371C
P 3600 3925
F 0 "R12" V 3680 3925 50  0000 C CNN
F 1 "100K" V 3600 3925 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3530 3925 50  0001 C CNN
F 3 "" H 3600 3925 50  0001 C CNN
	1    3600 3925
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 5B6B3722
P 2875 3450
F 0 "R7" V 2955 3450 50  0000 C CNN
F 1 "100K" V 2875 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2805 3450 50  0001 C CNN
F 3 "" H 2875 3450 50  0001 C CNN
	1    2875 3450
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 5B6B3728
P 2875 3650
F 0 "R8" V 2955 3650 50  0000 C CNN
F 1 "100K" V 2875 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2805 3650 50  0001 C CNN
F 3 "" H 2875 3650 50  0001 C CNN
	1    2875 3650
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 5B6B372E
P 3075 4125
F 0 "R10" V 3155 4125 50  0000 C CNN
F 1 "100K" V 3075 4125 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3005 4125 50  0001 C CNN
F 3 "" H 3075 4125 50  0001 C CNN
	1    3075 4125
	-1   0    0    1   
$EndComp
Wire Wire Line
	3025 3450 3150 3450
Wire Wire Line
	3025 3650 3150 3650
Wire Wire Line
	3125 3650 3125 3925
Wire Wire Line
	3125 3925 3450 3925
Connection ~ 3125 3650
Wire Wire Line
	3750 3925 3800 3925
Wire Wire Line
	3800 3925 3800 3550
Wire Wire Line
	3750 3550 3900 3550
Wire Wire Line
	3075 3975 3075 3450
Connection ~ 3075 3450
$Comp
L GND #PWR017
U 1 1 5B6B3741
P 3075 4375
F 0 "#PWR017" H 3075 4125 50  0001 C CNN
F 1 "GND" H 3075 4225 50  0000 C CNN
F 2 "" H 3075 4375 50  0001 C CNN
F 3 "" H 3075 4375 50  0001 C CNN
	1    3075 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 4275 3075 4375
$Comp
L LM324 U1
U 2 1 5B6B3748
P 2000 3650
F 0 "U1" H 2000 3850 50  0000 L CNN
F 1 "OPA4197" H 1950 3450 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1950 3750 50  0001 C CNN
F 3 "" H 2050 3850 50  0001 C CNN
	2    2000 3650
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5B6B374E
P 1525 3300
F 0 "R3" V 1605 3300 50  0000 C CNN
F 1 "100K" V 1525 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1455 3300 50  0001 C CNN
F 3 "" H 1525 3300 50  0001 C CNN
	1    1525 3300
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 5B6B3754
P 1525 3775
F 0 "R4" V 1605 3775 50  0000 C CNN
F 1 "100K" V 1525 3775 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1455 3775 50  0001 C CNN
F 3 "" H 1525 3775 50  0001 C CNN
	1    1525 3775
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 3750 1650 3750
Wire Wire Line
	1650 3750 1650 4025
Wire Wire Line
	1650 4025 2300 4025
Wire Wire Line
	2300 4025 2300 3650
Wire Wire Line
	2300 3650 2725 3650
Connection ~ 2300 3650
Wire Wire Line
	1525 3450 1525 3625
Wire Wire Line
	1700 3550 1525 3550
Connection ~ 1525 3550
$Comp
L GND #PWR018
U 1 1 5B6B3764
P 1525 4025
F 0 "#PWR018" H 1525 3775 50  0001 C CNN
F 1 "GND" H 1525 3875 50  0000 C CNN
F 2 "" H 1525 4025 50  0001 C CNN
F 3 "" H 1525 4025 50  0001 C CNN
	1    1525 4025
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 3925 1525 4025
$Comp
L VCC #PWR019
U 1 1 5B6B376B
P 1775 2975
F 0 "#PWR019" H 1775 2825 50  0001 C CNN
F 1 "VCC" H 1775 3125 50  0000 C CNN
F 2 "" H 1775 2975 50  0001 C CNN
F 3 "" H 1775 2975 50  0001 C CNN
	1    1775 2975
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR020
U 1 1 5B6B3771
P 1275 2975
F 0 "#PWR020" H 1275 2825 50  0001 C CNN
F 1 "VDD" H 1275 3125 50  0000 C CNN
F 2 "" H 1275 2975 50  0001 C CNN
F 3 "" H 1275 2975 50  0001 C CNN
	1    1275 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 3150 1525 3125
Wire Wire Line
	1275 2975 1275 3025
Wire Wire Line
	1775 2975 1775 3025
$Comp
L VCC #PWR021
U 1 1 5B6B377A
P 1900 3250
F 0 "#PWR021" H 1900 3100 50  0001 C CNN
F 1 "VCC" H 1900 3400 50  0000 C CNN
F 2 "" H 1900 3250 50  0001 C CNN
F 3 "" H 1900 3250 50  0001 C CNN
	1    1900 3250
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR022
U 1 1 5B6B3780
P 3350 3175
F 0 "#PWR022" H 3350 3025 50  0001 C CNN
F 1 "VCC" H 3350 3325 50  0000 C CNN
F 2 "" H 3350 3175 50  0001 C CNN
F 3 "" H 3350 3175 50  0001 C CNN
	1    3350 3175
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR023
U 1 1 5B6B3786
P 1900 4100
F 0 "#PWR023" H 1900 3950 50  0001 C CNN
F 1 "VDD" H 1900 4250 50  0000 C CNN
F 2 "" H 1900 4100 50  0001 C CNN
F 3 "" H 1900 4100 50  0001 C CNN
	1    1900 4100
	-1   0    0    1   
$EndComp
$Comp
L VDD #PWR024
U 1 1 5B6B378C
P 3350 4000
F 0 "#PWR024" H 3350 3850 50  0001 C CNN
F 1 "VDD" H 3350 4150 50  0000 C CNN
F 2 "" H 3350 4000 50  0001 C CNN
F 3 "" H 3350 4000 50  0001 C CNN
	1    3350 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 3850 3350 4000
Wire Wire Line
	1900 3950 1900 4100
Wire Wire Line
	1900 3250 1900 3350
Wire Wire Line
	3350 3175 3350 3250
Text GLabel 2600 3450 0    40   Input ~ 0
IN2
Text GLabel 3900 3550 2    40   Input ~ 0
OUT2
Wire Wire Line
	2600 3450 2725 3450
Connection ~ 3800 3550
$Comp
L Jumper_NC_Dual JP2
U 1 1 5B6B379A
P 1525 3025
F 0 "JP2" H 1575 2925 50  0000 L CNN
F 1 "-+" H 1525 3125 50  0000 C BNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 1525 3025 50  0001 C CNN
F 3 "" H 1525 3025 50  0001 C CNN
	1    1525 3025
	1    0    0    -1  
$EndComp
$EndSCHEMATC

/**
 * @file XPT2046.h
 *
 */

#ifndef XPT2046_H
#define XPT2046_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "../lvgl/lv_hal/lv_hal_indev.h"

/*********************
 *      DEFINES
 *********************/
#define XPT2046_HOR_RES     800
#define XPT2046_VER_RES     480
#define XPT2046_X_MIN       110
#define XPT2046_Y_MIN       1200
#define XPT2046_X_MAX       1920
#define XPT2046_Y_MAX       2400
#define XPT2046_AVG         4
#define XPT2046_X_INV       1
#define XPT2046_Y_INV       0
#define XPT2046_XY_SWAP		0

#define CMD_Y_READ  0b10010000
#define CMD_X_READ  0b11010000


/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/
void xpt2046_init(void);
bool XPT2046_Read(lv_indev_data_t * data);

/**********************
 *      MACROS
 **********************/


/*---------
 *  SPI
 *---------*/
// #define LV_DRV_INDEV_SPI_CS(val)            /*spi_cs_set(val)*/     /*Set the SPI's Chip select to 'val'*/
// #define LV_DRV_INDEV_SPI_XCHG_BYTE(data)    0 /*spi_xchg(val)*/     /*Write 'val' to SPI and give the read value*/




#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* XPT2046_H */

/*
 * lvSSD1963.c
 *
 *  Created on: 2018. dec. 27.
 *      Author: zoli
 *
 *  LittlevGL wrapper for the SSD1963 driver*
 *
 */

// library functions
void SSD1963_Flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
	SSD1963_Map(x1, y1, x2, y2, color_p);
	lv_flush_ready();
}

void SSD1963_Fill(int32_t x1, int32_t y1, int32_t x2, int32_t y2, lv_color_t color)
{
	// Return if the area is out the screen
	if (x2 < 0)
		return;
	if (y2 < 0)
		return;
	if (x1 > SSD1963_WIDTH - 1)
		return;
	if (y1 > SSD1963_HEIGHT - 1)
		return;

	// Truncate the area to the screen
	uint16_t act_x1 = x1 < 0 ? 0 : x1;
	uint16_t act_y1 = y1 < 0 ? 0 : y1;
	uint16_t act_x2 = x2 > SSD1963_WIDTH - 1 ? SSD1963_WIDTH - 1 : x2;
	uint16_t act_y2 = y2 > SSD1963_HEIGHT - 1 ? SSD1963_HEIGHT - 1 : y2;

	uint32_t size = (act_x2 - act_x1 + 1) * (act_y2 - act_y1 + 1);
	uint32_t i;

	uint16_t color16 = lv_color_to16(color);

	_SSD1963_SetArea(x1, y1, x2, y2);
	SSD1963_REG = SSD1963_CMD_WRITE_MEMORY_START;
	for (i = 0; i < size; i++)
	{
		SSD1963_RAM = color16;
	}
}

void SSD1963_Map(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
	// Return if the area is out the screen
	if (x2 < 0)
		return;
	if (y2 < 0)
		return;
	if (x1 > SSD1963_WIDTH - 1)
		return;
	if (y1 > SSD1963_HEIGHT - 1)
		return;

	// Truncate the area to the screen
	uint16_t act_x1 = x1 < 0 ? 0 : x1;
	uint16_t act_y1 = y1 < 0 ? 0 : y1;
	uint16_t act_x2 = x2 > SSD1963_WIDTH - 1 ? SSD1963_WIDTH - 1 : x2;
	uint16_t act_y2 = y2 > SSD1963_HEIGHT - 1 ? SSD1963_HEIGHT - 1 : y2;

	uint32_t size = (act_x2 - act_x1 + 1) * (act_y2 - act_y1 + 1);
	uint32_t i;

	_SSD1963_SetArea(x1, y1, x2, y2);
	SSD1963_REG = SSD1963_CMD_WRITE_MEMORY_START;
	for (i = 0; i < size; i++)
	{
		SSD1963_RAM = lv_color_to16(color_p[i]);
	}
}


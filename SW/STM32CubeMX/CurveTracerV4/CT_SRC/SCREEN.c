/*
 * SCREEN.c
 *
 *  Created on: 2018. nov. 27.
 *      Author: zoli
 */

#include <stdint.h>
#include "CurveTracer.h"
#include "lvgl.h"
#include "../lv_conf.h"
#include "SSD1963_fsmc.h"
// #include "XPT2046.h"
#include "tim.h"
#include "SCREEN.h"

#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "wfg.h"

// Temporary - [CT-14]
#include "data_acquisition.h"
#include "command.h"

volatile uint8_t lvgl_period_tick;
volatile uint8_t pin_state;

static lv_style_t lv_style_dark;
static lv_style_t lv_style_grid;
static lv_style_t lv_style_line_a;
lv_obj_t* screen;
lv_disp_drv_t disp_drv;

// grid data
const static lv_point_t grid_points[] = {
		{  0,   0}, {  0, 399}, {499, 399}, {499,   0}, {  0,   0}, {  0, 399}, { 49, 399}, { 49,   0}, { 99,   0}, { 99, 399},
		{149, 399}, {149,   0}, {199,   0}, {199, 399}, {249, 399}, {249,   0}, {299,   0}, {299, 399}, {349, 399}, {349,   0},
		{399,   0}, {399, 399}, {449, 399}, {449,   0}, {499,   0}, {  0,   0}, {  0,  49}, {499,  49}, {499,  99}, {	0, 99},
		{  0, 149}, {499, 149}, {499, 199}, {  0, 199}, {  0, 249}, {499, 249}, {499, 299}, {  0, 299}, {  0, 349}, {499, 349},
		{499, 399}};
lv_obj_t * scope_container;
lv_obj_t * scope_line_a;


void SCREEN_Init()
{
	// lv_obj_t* screen;
	lvgl_period_tick = 10;

	SSD1963_Init();

	lv_init();

	disp_drv.disp_flush = SSD1963_Flush;
	disp_drv.disp_fill = SSD1963_Fill;
	disp_drv.disp_map = SSD1963_Map;
	lv_disp_drv_register(&disp_drv);

/*
	lv_indev_drv_t indev_drv;                       // Descriptor of an input device driver
	lv_indev_drv_init(&indev_drv);                  // Basic initialization
	indev_drv.type = LV_INDEV_TYPE_POINTER;         // The touchpad is pointer type device
	indev_drv.read = XPT2046_Read;                  // Library ready your touchpad via this function
	lv_indev_drv_register(&indev_drv);              // Finally register the driver
*/


    // Dark screen style
    lv_style_dark.glass = 0;
    lv_style_dark.body.opa = LV_OPA_COVER;
    lv_style_dark.body.main_color = LV_COLOR_BLACK;
    lv_style_dark.body.grad_color = LV_COLOR_BLACK;
    lv_style_dark.body.radius = 0;
    lv_style_dark.body.padding.ver = LV_DPI / 12;
    lv_style_dark.body.padding.hor = LV_DPI / 12;
    lv_style_dark.body.padding.inner = LV_DPI / 12;

    lv_style_dark.body.border.color = LV_COLOR_BLACK;
    lv_style_dark.body.border.opa = LV_OPA_COVER;
    lv_style_dark.body.border.width = 0;
    lv_style_dark.body.border.part = LV_BORDER_FULL;

    lv_style_dark.body.shadow.color = LV_COLOR_GRAY;
    lv_style_dark.body.shadow.type = LV_SHADOW_FULL;
    lv_style_dark.body.shadow.width = 0;

    lv_style_dark.text.opa = LV_OPA_COVER;
    lv_style_dark.text.color = LV_COLOR_WHITE;
    lv_style_dark.text.font = LV_FONT_DEFAULT;
    lv_style_dark.text.letter_space = 2;
    lv_style_dark.text.line_space = 2;

    lv_style_dark.image.opa = LV_OPA_COVER;
    lv_style_dark.image.color = LV_COLOR_MAKE(0x20, 0x20, 0x20);
    lv_style_dark.image.intense = LV_OPA_TRANSP;

    lv_style_dark.line.opa = LV_OPA_COVER;
    lv_style_dark.line.color = LV_COLOR_MAKE(0x20, 0x20, 0x20);
    lv_style_dark.line.width = 2;
    lv_style_dark.line.rounded = 0;

    // Grid style
    lv_style_copy(&lv_style_grid, &lv_style_dark);
    lv_style_grid.line.color = LV_COLOR_MAKE(0x40,0x40,0x40);
    lv_style_grid.line.width = 1;
    // lv_style_grid.body.border.color = LV_COLOR_GRAY;
    // lv_style_grid.body.border.width = 1;

/*
    lv_style_copy(&lv_style_line_a, &lv_style_grid);
    lv_style_line_a.line.color = LV_COLOR_RED;
*/
    SCREEN_CreateLog(lv_scr_act());

}

volatile bool ScopeUpgradeEnabled = false;
volatile uint16_t ScopeUpgrade_period_tick = SCREEN_SCOPE_PERIOD_MS;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == htim7.Instance)
	{
		// LittlevGL tick
		lv_tick_inc(1);
		// LittlevGL period tick
		if(lvgl_period_tick == 0)
		{
			lv_task_handler();
			lvgl_period_tick = 10;
		}
		else
		{
			lvgl_period_tick--;
		}
		// Screen frame upgrade
		/*
		if(ScopeUpgradeEnabled)
		{
			if(ScopeUpgrade_period_tick == 0)
			{
				DAQ_GetData(CMD_COM_DEVICE_CON);
				ScopeUpgrade_period_tick = SCREEN_SCOPE_PERIOD_MS;
			}
			else
			{
				ScopeUpgrade_period_tick--;
			}
		}
		*/
	}
}

lv_obj_t * log_container;
lv_obj_t * scr_log;

void SCREEN_CreateLog(lv_obj_t * parent)
{
	log_container = lv_obj_create(parent, NULL);
	lv_obj_set_style(log_container, &lv_style_dark);
	lv_obj_set_size(log_container,800,480);
	lv_obj_set_pos(log_container,0,0);

	scr_log = lv_ta_create(log_container, NULL);
	lv_obj_set_style(scr_log, &lv_style_dark);
	lv_obj_set_size(scr_log,800,480);
	lv_obj_set_pos(scr_log,0,0);
	lv_ta_set_cursor_type(scr_log,LV_CURSOR_NONE);
	lv_ta_set_text(scr_log,"");
}
void SCREEN_Log(const char * logtext)
{
	lv_ta_add_text(scr_log, logtext);
}

void SCREEN_CreateScope()
{
	scope_container = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_style(scope_container, &lv_style_dark);
	lv_obj_set_size(scope_container,500,400);
	lv_obj_set_pos(scope_container,150,0);

	lv_obj_t * grid_line;
	grid_line = lv_line_create(scope_container, NULL);
	lv_line_set_auto_size(grid_line, false);
	lv_obj_set_size(grid_line,500,400);
	lv_obj_set_pos(grid_line,0,0);
	lv_line_set_style(grid_line, &lv_style_grid);
	lv_obj_align(grid_line, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);
	lv_line_set_points(grid_line, grid_points, 41);

	scope_line_a = lv_line_create(scope_container, NULL);
	lv_line_set_auto_size(scope_line_a, false);
	lv_obj_set_size(scope_line_a,500,400);
	lv_obj_set_pos(scope_line_a,0,0);
	lv_line_set_style(scope_line_a, &lv_style_line_a);
	lv_obj_align(scope_line_a, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);
	// lv_line_set_points(grid_line, grid_points, 41);

	// Start scope timer
	ScopeUpgradeEnabled = true;
}

static lv_point_t scope_display_tbl0[WFG_MAX_SAMPLES];
static lv_point_t scope_display_tbl1[WFG_MAX_SAMPLES];
lv_point_t * scope_curr_line_tbl;
uint8_t scope_curr_line_store = 0;
void SCREEN_Scope_Setup()
{
	if(scope_curr_line_store == 0)
	{
		scope_curr_line_store = 1;
		scope_curr_line_tbl = (lv_point_t *)scope_display_tbl1;
	}
	else
	{
		scope_curr_line_store = 0;
		scope_curr_line_tbl = (lv_point_t *)scope_display_tbl0;
	}
}

void SCREEN_Scope_AddPoint(uint16_t index, uint16_t voltage, uint16_t current)
{
	scope_curr_line_tbl[index].x = voltage / 10;
	scope_curr_line_tbl[index].y = current / 10;
}

void SCREEN_Scope_Display()
{
	lv_line_set_points(scope_line_a, scope_curr_line_tbl, CT_NV_Settings.wfgNumSamples);
}


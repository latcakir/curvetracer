/*
 * MCP45HVx1.c
 *
 *  Created on: 2018. aug. 26.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include "i2c.h"
#include "MCP45HVx1.h"

void MCP45HVx1_SetWiper(uint16_t addr, uint8_t data)
{
	uint8_t buff[2] = {0,data};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 2, 100);
}

/*
 * usb_fifo.c
 *
 *  Created on: 2018. aug. 22.
 *      Author: zoli
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "usb_device.h"
#include "usbd_def.h"
#include "usbd_cdc_if.h"
#include "usb_fifo.h"

#include "command.h"


bool USB_CDC_Enabled = false;

/*
FIFO RX_FIFO = {.head=0, .tail=0};

// returns bytes read (could be zero)
// would be easy to make it end early on a stop char (e.g., \r or \n)
uint8_t VCP_read(uint8_t* Buf, uint32_t Len)
{
  uint32_t count=0;
// Check inputs
  if ((Buf == NULL) || (Len == 0))
 {
  return 0;
 }

while (Len--)
{
if (RX_FIFO.head==RX_FIFO.tail) return count;
count++;
*Buf++=RX_FIFO.data[RX_FIFO.tail];
RX_FIFO.tail=FIFO_INCR(RX_FIFO.tail);
}

return count;
}
*/

uint8_t USB_CDC_RX_numlines = 0;
FIFO _rx_fifo = {.head=0, .tail=0};

bool USB_CDC_RX_full()
{
	return FIFO_INCR(_rx_fifo.head) == _rx_fifo.tail;
}


void USB_CDC_RX_Enqueue(uint8_t value)
{
	_rx_fifo.data[_rx_fifo.head]=value;
	if(value == '\r') USB_CDC_RX_numlines++;
	_rx_fifo.head=FIFO_INCR(_rx_fifo.head);
}

uint8_t USB_CDC_RX_read(uint8_t* Buf, uint32_t Len)
{
	uint8_t count = 0;
	if(Buf == NULL) return 0;
	for(;_rx_fifo.head != _rx_fifo.tail && Len > 0; Len--)
	{
		count++;
		*Buf++ = _rx_fifo.data[_rx_fifo.tail];
		_rx_fifo.tail = FIFO_INCR(_rx_fifo.tail);
	}
	return count;
}

uint8_t USB_CDC_RX_readln(uint8_t* Buf)
{
	uint8_t count = 0;
	if(Buf == NULL) return 0;
	for(;_rx_fifo.head != _rx_fifo.tail;count++)
	{
		if(_rx_fifo.data[_rx_fifo.tail] == '\r')
		{
			_rx_fifo.tail = FIFO_INCR(_rx_fifo.tail);
			if(USB_CDC_RX_numlines > 0)
			{
				USB_CDC_RX_numlines--;
			}
			*Buf++ = 0;
			count++;
			return count;
		}
		*Buf++ = _rx_fifo.data[_rx_fifo.tail];
		_rx_fifo.tail = FIFO_INCR(_rx_fifo.tail);
	}
//	*Buf++ = 0;
//	count++;
	return count;
}


void USB_CDC_TX_write(uint8_t* Buf,uint16_t len)
{
	if(hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED)
	{
		CDC_Transmit_FS(Buf, len);
	}
}

void USB_CDC_RX_WaitforReady()
{
	uint16_t i;
	if(USB_CDC_Enabled)
	{
		for(i=0; i < USB_CDC_TIMEOUT; i++)
		{
			if(CDC_TX_State() == USBD_OK) break;
			HAL_Delay(1);
		}
		// Disable if timeout expired
		USB_CDC_Enabled = i < USB_CDC_TIMEOUT;
	}
}

// ----------------------------USB Connection Control --------------------
void USB_CDC_Connect()
{
	// Enable communication
	USB_CDC_Enabled = true;
}

/// Command descriptor for WFG_SetFreq

void USB_CDC_CMD_Connect(uint8_t source, void* args[], uint8_t len)
{
	USB_CDC_Connect();
}

const CMD_CommandTypeDef USB_CDC_CMD_Connect_Descriptor =
{
	.CommandCode = CMD_CODE_USB_CDC_CONNECT,
	.NumParams = 0,
	.CommandProcessor = &USB_CDC_CMD_Connect,
	.ParamArr = NULL
};

void USB_CDC_Disconnect()
{
	// Enable communication
	USB_CDC_Enabled = false;
}

/// Command descriptor for WFG_SetFreq

void USB_CDC_CMD_Disconnect(uint8_t source, void* args[], uint8_t len)
{
	USB_CDC_Disconnect();
}

const CMD_CommandTypeDef USB_CDC_CMD_Disconnect_Descriptor =
{
	.CommandCode = CMD_CODE_USB_CDC_DISCONNECT,
	.NumParams = 0,
	.CommandProcessor = &USB_CDC_CMD_Disconnect,
	.ParamArr = NULL
};


void USB_CDC_Init()
{
	CMD_RegisterCommand(USB_CDC_CMD_Connect_Descriptor);
	CMD_RegisterCommand(USB_CDC_CMD_Disconnect_Descriptor);
}

/*
 * poweramp.c
 *
 *  Created on: 2018. aug. 26.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include "poweramp.h"
// #include <math.h>
#include "MCP45HVx1.h"
#include "command.h"
#include "CurveTracer.h"
#include <stdio.h>

const uint8_t pa_preset_vpp[25] =
{
		0,	// 0V
		2,	// 200mV
		4,	// 400mV
		6,	// 600mV
		8,	// 800mV
		10,	// 1V
		20,	// 2V
		30,	// 3V
		40,	// 4V
		50,	// 5V
		60,	// 6V
		70,	// 7V
		80,	// 8V
		90,	// 9V
		100,	// 10V
		110,	// 11V
		120,	// 12V
		130,	// 13V
		140,	// 14V
		150,	// 15V
		160,	// 16V
		170,	// 17V
		180,	// 18V
		190,	// 19V
		200		// 20V
};


double PA_SetWiper(uint8_t wiper)
{
	CT_NV_Settings.paWiper = wiper;
	CT_NV_Save();
	MCP45HVx1_SetWiper(I2C_ADDR_PA_DIGIPOT, wiper);
	CT_V_Settings.paVpp = (double)wiper * 0.1;

	return PA_GetVpp();
}

void PA_CMD_SetWiper(uint8_t source, void* args[], uint8_t len)
{
	PA_SetWiper(*((uint8_t*)args[0]));
}

const uint8_t PA_CMD_SetWiper_Params[1] = {CMD_TYPE_UINT8};

const CMD_CommandTypeDef PA_CMD_SetWiper_Descriptor =
{
	.CommandCode = CMD_CODE_PA_SETWIPER,
	.NumParams = 1,
	.CommandProcessor = &PA_CMD_SetWiper,
	.ParamArr = PA_CMD_SetWiper_Params
};

double PA_SetVpp(double Vpp)
{
	return PA_SetWiper((uint8_t)(Vpp * 10));
}

void PA_CMD_SetVpp(uint8_t source, void* args[], uint8_t len)
{
	PA_SetVpp(*((double*)args[0]));
}

const uint8_t PA_CMD_SetVpp_Params[1] = {CMD_TYPE_DOUBLE};

const CMD_CommandTypeDef PA_CMD_SetVpp_Descriptor =
{
	.CommandCode = CMD_CODE_PA_SETVPP,
	.NumParams = 1,
	.CommandProcessor = &PA_CMD_SetVpp,
	.ParamArr = PA_CMD_SetVpp_Params
};

double PA_SelVpp(uint8_t index)
{
	return PA_SetWiper(pa_preset_vpp[index]);
}

void PA_CMD_SelVpp(uint8_t source, void* args[], uint8_t len)
{
	PA_SelVpp(*((uint8_t*)args[0]));
}

const uint8_t PA_CMD_SelVpp_Params[1] = {CMD_TYPE_UINT8};

const CMD_CommandTypeDef PA_CMD_SelVpp_Descriptor =
{
	.CommandCode = CMD_CODE_PA_SELVPP,
	.NumParams = 1,
	.CommandProcessor = &PA_CMD_SelVpp,
	.ParamArr = PA_CMD_SelVpp_Params
};

double PA_GetVpp()
{
	CMD_Response(CMD_SEND_PA_VPP_T, CMD_SEND_PA_VPP,CT_V_Settings.paVpp);
	return CT_V_Settings.paVpp;
}

/// Command descriptor for PA_GetVpp

void PA_CMD_GetVpp(uint8_t source, void* args[], uint8_t len)
{
	PA_GetVpp();
}

const CMD_CommandTypeDef PA_CMD_GetVpp_Descriptor =
{
	.CommandCode = CMD_CODE_PA_GETVPP,
	.NumParams = 0,
	.CommandProcessor = &PA_CMD_GetVpp,
	.ParamArr = NULL
};

void PA_CMD_Register()
{
	CMD_RegisterCommand(PA_CMD_SetWiper_Descriptor);
	CMD_RegisterCommand(PA_CMD_SetVpp_Descriptor);
	CMD_RegisterCommand(PA_CMD_SelVpp_Descriptor);
	CMD_RegisterCommand(PA_CMD_GetVpp_Descriptor);
}


void PA_Init()
{
	PA_CMD_Register();
	PA_SetWiper(CT_NV_Settings.paWiper);
}

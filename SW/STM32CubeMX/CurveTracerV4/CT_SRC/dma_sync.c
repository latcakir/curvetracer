/*
 * dma_sync.c
 *
 *  Created on: Aug 30, 2018
 *      Author: gomorzo
 *
 *  This code is used to synchronize various functions to the DAC DMA.
 *  DUT switching, DAC Table switching must be done at zero crossing
 *  The pulse generator, sync output must be synchronized also
 */


#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_dma.h"
#include "stm32f4xx_hal_adc.h"
#include "dma_sync.h"
#include "dac.h"
#include "gpio.h"
#include "attenuator.h"
#include "data_acquisition.h"


///
/// HAL_DAC_ConvHalfCpltCallbackCh1 - Called at DAC DMA cycle half complete - weak override
void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	// GPIO for Sync OUT
	HAL_GPIO_WritePin(SYNC_OUT_GPIO_Port, SYNC_OUT_Pin, GPIO_PIN_RESET);
}

///
/// HAL_DAC_ConvCpltCallbackCh1 - Called at DAC DMA cycle complete - weak override
void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	// GPIO for Sync OUT
	HAL_GPIO_WritePin(SYNC_OUT_GPIO_Port, SYNC_OUT_Pin, GPIO_PIN_SET);
// 	HAL_GPIO_TogglePin(SYNC_OUT_GPIO_Port, SYNC_OUT_Pin);
	// Alternate DUT
	ATT_DUT_Switcher();
	// Sine table switch
	// Start pulse generator
	// Start ADC
	DAQ_Start();
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	// Process ADC data
	DAQ_Process();
}


/*
void DS_Init()
{
	// register the DMA interrupt callback functions (used for synchronization called at half and full DMA cycles
	hdac.DMA_Handle1->XferCpltCallback = DS_DMA_Complette;
	hdac.DMA_Handle1->XferHalfCpltCallback = DS_DMA_HalfComplette;

}
*/

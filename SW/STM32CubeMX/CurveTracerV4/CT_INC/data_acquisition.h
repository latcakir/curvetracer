/*
 * data_acquisition.h
 *
 *  Created on: 2018. okt. 13.
 *      Author: zoli
 */

#ifndef DATA_ACQUISITION_H_
#define DATA_ACQUISITION_H_

void DAQ_Start();
void DAQ_Init();
void DAQ_Process();
void DAQ_Process_Worker();

// Temporary until the binary data processor is written [CT-14]
void DAQ_GetData(uint8_t source);

#endif /* DATA_ACQUISITION_H_ */

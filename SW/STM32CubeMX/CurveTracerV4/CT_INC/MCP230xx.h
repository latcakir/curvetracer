/*
 * MCP230xx.h
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#ifndef MCP230XX_H_
#define MCP230XX_H_

#define MCP23008	0
#define MCP23017	1

void MCP23008_Write(uint16_t addr, uint8_t data);
void MCP23017_Write(uint16_t addr, uint8_t port, uint8_t data);
void MCP23017_WriteW(uint16_t addr, uint16_t data);
void MCP23008_Init(uint16_t addr);
void MCP23017_Init(uint16_t addr);


#endif /* MCP230XX_H_ */

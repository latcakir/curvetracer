﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    public class PresetElement
    {
        public PresetElement(float value, UInt16 presetIndex)
        {
            this.value = value;
            this.label = value.ToString();
            this.presetIndex = presetIndex;

        }
        public PresetElement(string label, UInt16 presetIndex)
        {
            this.label = label;
            this.presetIndex = presetIndex;

        }
        public override string ToString()
        {
            return this.label;
        }
        public float value;
        public UInt16 presetIndex;
        public string label;
    }
}

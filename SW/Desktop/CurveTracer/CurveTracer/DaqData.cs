﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    public class DaqData
    {
        public UInt16 samplenum;
        public UInt16 voltage;
        public UInt16 current;
        public bool valid;
        DaqData()
        {
            valid = false;
        }
        DaqData(string data)
        {
            valid = false;
            _parse(data);
        }
        void _parse(string data)
        {
            byte[] binaryData = UnicodeEncoding.Unicode.GetBytes(data);
            bool dataValid = (data.Length == 5); // we need exactly 5 byte data

            // check if the first 4 bytes between 128 and 255
            if(dataValid)
            {
                for (int i = 0; i < 8; i+=2)
                {
                    dataValid = ((UInt16)char.GetNumericValue(data[i]) > 128 && (UInt16)char.GetNumericValue(data[i]) < 257);
                    if (!dataValid) break;
                }
            }
            // check if the last byte is between 64 and 128


            if (data.Length == 5) 
            {
                dataValid = true;
            }
        }
    }
}

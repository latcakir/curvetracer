#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

#include <avr/pgmspace.h>

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

#define LCD_RESET A4  // Can alternately just connect to Arduino's reset pin

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GREY  0x9999

#define FASTADC 1
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif


Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
//------------------------------------------------------------------------
int y=0;
int sensorValue1 = 0;        // value read from the pot
int outputValue1 = 0;
int sensorValue2 = 0;        // value read from the pot
int outputValue2 = 0;
int outputValueM = 0;
int x=0;
int t=0;
int max1=400;
int min1=400;
int maxR=0;
int minR=0;

const int analogInPin1 = A5;
const int analogInPin2 = A7;

int value[240];
int value2[240];
int i=0;


// save some unsigned ints
byte valueC[240];
byte valueC2[240];
const int buttonPin = 12;
int buttonState = 0;
//-----------------------------------------------------------------------
void setup(void) {
  Serial.begin(9600);
  tft.fillScreen(BLACK);
  pinMode(buttonPin, INPUT);
#if FASTADC
  // set prescale to 16
  sbi(ADCSRA,ADPS2) ;
  cbi(ADCSRA,ADPS1) ;
  cbi(ADCSRA,ADPS0) ;
#endif

#ifdef USE_ADAFRUIT_SHIELD_PINOUT
  progmemPrintln(PSTR("Using Adafruit 2.8\" TFT Arduino Shield Pinout"));
#else
  progmemPrintln(PSTR("Using Adafruit 2.8\" TFT Breakout Board Pinout"));
#endif

  tft.reset();

  uint16_t identifier = tft.readID();

  tft.begin(identifier);

  tft.setRotation(1);
  tft.setCursor(50, 50);
    tft.setTextColor(WHITE);  tft.setTextSize(10);
    tft.println("<TM>");
    tft.setCursor(50, 130);
    tft.setTextColor(WHITE);  tft.setTextSize(2);
    tft.println("Digital curve tracer");
    tft.setCursor(120, 220);
    tft.setTextColor(WHITE);  tft.setTextSize(1);
    tft.println("Copyright Mate Toth");
    delay(3000);
    tft.fillScreen(BLACK);

   tft.setRotation(1);
    tft.drawLine(120, 0, 120, 239, WHITE);
    tft.drawLine(0, 120, 239, 120, WHITE);
    tft.drawLine(239, 0, 239, 239, WHITE);
    tft.drawLine(0, 0, 0, 239, WHITE);
    tft.drawLine(0, 0, 239, 0, WHITE);
    tft.drawLine(0, 239, 239, 239, WHITE);

    tft.setCursor(250, 180);
    tft.setTextColor(YELLOW);  tft.setTextSize(2);
    tft.println("SAMP.");
    tft.setCursor(250, 210);
    tft.setTextColor(RED);
    tft.println("SAVED");
}

void loop(void) {
    tft.setRotation(1);
    tft.drawLine(120, 0, 120, 239, WHITE);
    tft.drawLine(0, 120, 239, 120, WHITE);
    buttonState = digitalRead(buttonPin);   //button state
    
    for (i=0;i<240;i++)                     //sampling from analog pins
    {
    value[i]=analogRead(analogInPin1);
    value2[i]=analogRead(analogInPin2);
    for(t=0;t<=130;t++);                     //trigger (Set to 50Hz)
    }
         
    for (i=0;i<240;i++)                     //display
   {

    outputValue1 = map(value[i], 0, 800, 0, 239);  //Y
    outputValue2 = map(value2[i], 0, 800, 0, 239); //X 
    outputValueM = map(value2[i], 0, 800, 0, 800);
    if( outputValueM>max1)                 //Calculating max and min points
    {
      max1= outputValueM;
      }
    if( outputValueM<min1)
    {
      min1= outputValueM;
      }  
    tft.fillCircle(outputValue2, outputValue1, 0, YELLOW);  //Measured line display
     
     if (buttonState == HIGH) {                             //Save the measured line (yellow)

        valueC[i]=outputValue1;
        valueC2[i]=outputValue2;
    
     }
    tft.fillCircle(valueC2[i], valueC[i], 0, RED);        //Saved line display
    //tft.fillCircle(outputValue1, i, 0, YELLOW);
    //tft.fillCircle(outputValue2, i, 0, BLUE);
    
   }
   for (i=0;i<240;i++)                    //clear lines, Display the same points as yellow and red lines, but use Black color to clear the display. It is much faster than clear the whole display.
   {
  
    outputValue1 = map(value[i], 0, 800, 0, 239);
    outputValue2 = map(value2[i], 0, 800, 0, 239);
    tft.fillCircle(outputValue2, outputValue1, 0, BLACK);
    tft.fillCircle(valueC2[i], valueC[i], 0, BLACK);
    //tft.fillCircle(outputValue1, i, 0, BLACK);
    //tft.fillCircle(outputValue2, i, 0, BLACK);
    
   }
   tft.setCursor(270, 10);
   tft.setTextColor(BLACK);  tft.setTextSize(2);
   tft.println(maxR);
   tft.setCursor(270, 50);
   tft.setTextColor(BLACK);  tft.setTextSize(2);
   tft.println(minR);
   max1=(max1-400)*(3000/400);
   min1=(400-min1)*(3000/400);
   tft.setCursor(270, 10);
   tft.setTextColor(WHITE);  tft.setTextSize(2);
   tft.println(max1);
   maxR=max1;
   tft.setCursor(270, 50);
   tft.println(min1);
   minR=min1;
   max1=400;
   min1=400;

   
}


// Copy string from flash to serial port
// Source string MUST be inside a PSTR() declaration!
void progmemPrint(const char *str) {
  char c;
  while(c = pgm_read_byte(str++)) Serial.print(c);
}

// Same as above, with trailing newline
void progmemPrintln(const char *str) {
  progmemPrint(str);
  Serial.println();
}


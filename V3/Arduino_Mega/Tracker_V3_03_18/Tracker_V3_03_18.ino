// the setup function runs once when you press reset or power the board
#include "TimerOne.h"
#include <SPI.h>

#define FASTADC 1
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

int buttonState = 0;
//int menubutton = 5;
int i = 0;
int result = 6;
int M1 = 0;
int M2 = 0;
int M3 = 0;
int M4 = 0;
int M5 = 0;
int M6 = 1;

int MF1 = 1;
int MF2 = 0;
int resultF = 1;
int MFR1 = 1;
int MFR2 = 0;
int MFR3 = 0;
int resultFR = 1;

int t = 1;
long int analog;
long int ti = 500000;

int I = 0;
int te = 0;

int valueX[270];
int valueY[270];

const int CS = 48;
const int CSV = 49;
const int CSI = 46;
int value5 = 100;
//---------------------------------Setup--------------------------------
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  Serial.begin(9600);
  SPI.begin();
#if FASTADC
  // set prescale to 16
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;
#endif

  for (int inc = 6; inc <= 13; inc++)
  {
    pinMode(inc, OUTPUT);
  }
  for (int inc = 22; inc <= 44; inc = inc + 2)
  {
    pinMode(inc, OUTPUT);
  }
  for (int inc = 23; inc <= 45; inc = inc + 2)
  {
    pinMode(inc, INPUT);
  }
  pinMode(CS, OUTPUT);
  pinMode(CSV, OUTPUT);
  pinMode(CSI, OUTPUT);
  pinMode(3, OUTPUT);
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(12, LOW);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, LOW);

  digitalWrite(34, HIGH);
  digitalWrite(42, HIGH);
  digitalWrite(44, HIGH);
  digitalWrite(40, HIGH);
  digitalWrite(38, HIGH);
  digitalWrite(36, HIGH);


}

void callback()
{
  if (t == 1)
  {
    digitalWrite(34, HIGH);
    digitalWrite(44, LOW);
    digitalWrite(13, HIGH);
    digitalWrite(12, LOW);
    t++;
  }
  else
  {
    digitalWrite(34, LOW);
    digitalWrite(44, HIGH);
    digitalWrite(13, LOW);
    digitalWrite(12, HIGH);
    t = 1;
  }

}
//-------------------------------------button---------------------------
int menubutton()
{
  for (i = 35; i <= 45; i = i + 2)
  {
    if (digitalRead(i) == 1)
    {
      if (i == 45) result = 6;
      if (i == 43) result = 5;
      if (i == 41) result = 4;
      if (i == 39) result = 3;
      if (i == 37) result = 2;
      if (i == 35) result = 1;

      //Serial.print(i);
      //delay(100);
    }
  }
  if (digitalRead(33) == 1)//A
  {
    resultF = 1;
  }

  if (digitalRead(23) == 1)//B
  {
    resultF = 2;
  }

  if (digitalRead(25) == 1)//A/B
  {
    resultF = 3;
  }
  //----------------Freq---------------
  if (digitalRead(27) == 1)//120
  {
    resultFR = 1;
  }

  if (digitalRead(29) == 1)//1000
  {
    resultFR = 2;
  }

  if (digitalRead(31) == 1)//2000
  {
    resultFR = 3;
  }
  return result;
}

//------------------------------------------------Sets the digital potentiometers-------------------------------------
void MCP41010Write(byte value)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte
  digitalWrite(CS, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(value);     // This tells it the pot position
  digitalWrite(CS, HIGH);
}
//--------------------------------------------Sets the V pot----------------------------------------------------------
void MCP41010WriteV(byte valueV)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte

  digitalWrite(CSV, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(valueV);     // This tells it the pot position
  digitalWrite(CSV, HIGH);
}
//--------------------------------------------Sets the I pot----------------------------------------------------------
void MCP41010WriteI(byte valueI)
{
  // Note that the integer vale passed to this subroutine
  // is cast to a byte

  digitalWrite(CSI, LOW);
  SPI.transfer(B00010001); // This tells the chip to set the pot
  SPI.transfer(valueI);     // This tells it the pot position
  digitalWrite(CSI, HIGH);
}

// the loop function runs over and over again forever
void loop() {

  menubutton();
  //------------------------------------Timer for A/B channel------------------------------------
  switch (resultF) {
    case 1:         //---------------------------A--------------------
      if (MF1 == 1)
      {
        detachInterrupt(callback);
        cli();
        digitalWrite(13, HIGH);
        digitalWrite(12, LOW);
        digitalWrite(34, LOW);
        digitalWrite(44, HIGH);
        digitalWrite(42, HIGH);
        MF1 = 0;
        MF2 = 1;
      }
      break;
    case 2:         //---------------------------B--------------------
      if (MF2 == 1)
      {
        detachInterrupt(callback);
        cli();
        digitalWrite(13, LOW);
        digitalWrite(12, HIGH);
        digitalWrite(34, HIGH);
        digitalWrite(44, LOW);
        digitalWrite(42, HIGH);
        MF1 = 1;
        MF2 = 0;
      }
      break;
    case 3:         //---------------------------A/B--------------------
      sei();
      digitalWrite(42, LOW);
      analog = analogRead(A15);
      ti = (analog + 50) * 800;
      Timer1.initialize(ti);         // initialize timer1, and set a 1/2 second period
      Timer1.attachInterrupt(callback);  // attaches callback() as a timer overflow interrupt
      MF1 = 1;
      MF2 = 1;
      break;

  }
  Serial.println(resultF);

  //------------------------------------Frequency------------------------------------
  switch (resultFR) {
    case 1:         //---------------------------120Hz--------------------
      if (MFR1 == 1)
      {
        analogWrite(3, 1);
        digitalWrite(36, HIGH);
        digitalWrite(38, HIGH);
        digitalWrite(40, LOW);
        MFR1 = 0;
        MFR2 = 1;
        MFR3 = 1;
      }
      break;
    case 2:         //---------------------------1000Hz--------------------
      if (MFR2 == 1)
      {
        analogWrite(3, 120);
        digitalWrite(36, HIGH);
        digitalWrite(38, LOW);
        digitalWrite(40, HIGH);
        MFR1 = 1;
        MFR2 = 0;
        MFR3 = 1;
      }
      break;
    case 3:         //---------------------------2000Hz--------------------
      if (MFR3 == 1)
      {
        analogWrite(3, 255);
        digitalWrite(36, LOW);
        digitalWrite(38, HIGH);
        digitalWrite(40, HIGH);
        MFR1 = 1;
        MFR2 = 1;
        MFR3 = 0;
      }

      break;

  }
  //---------------------------------------PC reader----------------------------------------------
  /*for (I = 0; I < 100; I++)               //sampling from analog pins
    {
    valueX[I] = analogRead(A0);
    valueY[I] = analogRead(A1);
    for (te = 0; te <= 500; te++);              //trigger (Set to 50Hz)
    }
    Serial.println("CLEARDATA");
    for (I = 0; I < 100; I++)               //sampling from analog pins
    {
    Serial.print("DATA,");
    Serial.print(valueX[I]);
    Serial.print(",");
    delay(50);
    Serial.print(valueY[I]);
    Serial.println();
    delay(50);
    }
    delay(1000);*/
  /*value5 = analogRead(A15);
    value5 = map(value5, 0, 1024, 0, 255);
    Serial.println(value5);
    MCP41010WriteI(value5);*/
  switch (result) {
    case 1:         //---------------------------VR1_VERY_LOW--------------------
      if (M1 == 1)
      {
        digitalWrite(32, LOW);//------------------LEDS--------------------
        digitalWrite(30, HIGH);
        digitalWrite(28, HIGH);
        digitalWrite(26, HIGH);
        digitalWrite(24, HIGH);
        digitalWrite(22, HIGH);

        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, HIGH);
        M1 = 0;
        M2 = 1;
        M3 = 1;
        M4 = 1;
        M5 = 1;
        M6 = 1;
        MCP41010WriteI(4);
        MCP41010Write(5);
        MCP41010WriteV(0);


      }
      break;

    case 2:         //---------------------------VR2_LOW--------------------
      if (M2 == 1)
      {
        digitalWrite(32, HIGH);//------------------LEDS--------------------
        digitalWrite(30, LOW);
        digitalWrite(28, HIGH);
        digitalWrite(26, HIGH);
        digitalWrite(24, HIGH);
        digitalWrite(22, HIGH);

        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, HIGH);
        digitalWrite(11, LOW);
        M1 = 1;
        M2 = 0;
        M3 = 1;
        M4 = 1;
        M5 = 1;
        M6 = 1;
        MCP41010WriteI(4);
        MCP41010Write(29);
        //Serial.print(M2);
        MCP41010WriteV(3);

      }
      break;

    case 3:         //---------------------------VR3_MED_1--------------------
      if (M3 == 1)
      {
        digitalWrite(32, HIGH);//------------------LEDS--------------------
        digitalWrite(30, HIGH);
        digitalWrite(28, LOW);
        digitalWrite(26, HIGH);
        digitalWrite(24, HIGH);
        digitalWrite(22, HIGH);

        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, HIGH);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        M1 = 1;
        M2 = 1;
        M3 = 0;
        M4 = 1;
        M5 = 1;
        M6 = 1;
        MCP41010WriteI(1);
        MCP41010Write(52);
        MCP41010WriteV(6);

      }
      break;

    case 4:         //---------------------------VR4_MED_2--------------------
      if (M4 == 1)
      {
        digitalWrite(32, HIGH);//------------------LEDS--------------------
        digitalWrite(30, HIGH);
        digitalWrite(28, HIGH);
        digitalWrite(26, LOW);
        digitalWrite(24, HIGH);
        digitalWrite(22, HIGH);

        digitalWrite(6, LOW);
        digitalWrite(7, LOW);
        digitalWrite(8, HIGH);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        M1 = 1;
        M2 = 1;
        M3 = 1;
        M4 = 0;
        M5 = 1;
        M6 = 1;
        MCP41010WriteI(1);
        MCP41010Write(110);
        MCP41010WriteV(12);

      }
      break;

    case 5:         //---------------------------VR5_HIGH--------------------
      if (M5 == 1)
      {
        digitalWrite(32, HIGH);//------------------LEDS--------------------
        digitalWrite(30, HIGH);
        digitalWrite(28, HIGH);
        digitalWrite(26, HIGH);
        digitalWrite(24, LOW);
        digitalWrite(22, HIGH);

        digitalWrite(6, LOW);
        digitalWrite(7, HIGH);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        M1 = 1;
        M2 = 1;
        M3 = 1;
        M4 = 1;
        M5 = 0;
        M6 = 1;
        MCP41010WriteI(1);
        MCP41010Write(189);
        MCP41010WriteV(22);

      }
      break;

    case 6:         //---------------------------VR6_VERY_HIGH--------------------
      if (M6 == 1)
      {
        digitalWrite(32, HIGH);//------------------LEDS--------------------
        digitalWrite(30, HIGH);
        digitalWrite(28, HIGH);
        digitalWrite(26, HIGH);
        digitalWrite(24, HIGH);
        digitalWrite(22, LOW);

        digitalWrite(6, HIGH);
        digitalWrite(7, LOW);
        digitalWrite(8, LOW);
        digitalWrite(9, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        M1 = 1;
        M2 = 1;
        M3 = 1;
        M4 = 1;
        M5 = 1;
        M6 = 0;
        MCP41010WriteI(1);
        MCP41010Write(254);
        MCP41010WriteV(30);

      }
      break;

      //Serial.println("");
  }
  //Serial.println(result);
}
